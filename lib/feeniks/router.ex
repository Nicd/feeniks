defmodule Feeniks.Router do
  import PlugHelpers

  @behaviour Plug

  @common_browser pipeline([
                    &Plug.Conn.fetch_session/1,
                    &Phoenix.Controller.fetch_flash/1,
                    &__MODULE__.put_root_layout/1,
                    &Phoenix.Controller.put_secure_browser_headers/1,
                    &Phoenix.Controller.protect_from_forgery/1
                  ])

  @common_api pipeline([
                &__MODULE__.accepts_json/1
              ])

  @index_action module_plug(ControllerPlug, &Feeniks.Controller.index/2)
  @index @common_browser ++ [@index_action]
  @index_api @common_api ++ [@index_action]

  @impl Plug
  def init(opts), do: opts

  @impl Plug
  def call(conn, _opts) do
    path_info = Plug.Router.Utils.decode_path_info!(conn)
    match(conn, conn.method, path_info)
  end

  def put_root_layout(conn) do
    Phoenix.Controller.put_root_layout(conn, {Feeniks.LayoutView, :root})
  end

  def accepts_json(conn) do
    Phoenix.Controller.accepts(conn, ["json"])
  end

  @spec match(Plug.Conn.t(), Plug.Conn.method(), Plug.Conn.segments()) :: Plug.Conn.t()
  defp match(conn, method, path_info)

  defp match(conn, "GET", _) do
    conn
    |> run_plugs(@index)
  end

  defp match(conn, "POST", _) do
    conn
    |> run_plugs(@index_api)
  end

  defp match(conn, _, _) do
    conn |> common_browser() |> Plug.Conn.put_status(200) |> Phoenix.Controller.text("Not found")
  end

  defp common_browser(conn) do
    run_plugs(conn, @common_browser)
  end
end
