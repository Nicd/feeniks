defmodule Feeniks.Endpoint do
  import PlugHelpers

  @behaviour Plug

  @config Phoenix.Endpoint.Supervisor.config(:feeniks, __MODULE__)
  @code_reloading @config[:code_reloader]

  @session_options [
    store: :cookie,
    key: "_feeniks_key",
    signing_salt: "örgölbörgöl"
  ]

  @plugs pipeline([
           &__MODULE__.set_endpoint/1,
           module_plug(Plug.Static,
             at: "/",
             from: :feeniks,
             gzip: true,
             only: ~w(app.css favicon.ico robots.txt)
           ),
           if @code_reloading do
             [
               module_plug(Phoenix.LiveReloader),
               module_plug(Phoenix.CodeReloader)
             ]
           end,
           module_plug(Plug.RequestId),
           module_plug(Plug.Logger),
           module_plug(Plug.Parsers,
             parsers: [:urlencoded, :multipart, :json],
             pass: ["*/*"],
             json_decoder: Jason
           ),
           module_plug(Plug.MethodOverride),
           module_plug(Plug.Head),
           module_plug(
             Plug.Session,
             @session_options
           ),
           module_plug(Feeniks.Router)
         ])

  @impl Plug
  def init(opts), do: opts

  @impl Plug
  def call(conn, _opts) do
    run_plugs(conn, @plugs)
  end

  def config(key) do
    @config[key]
  end

  def set_endpoint(conn), do: Plug.Conn.put_private(conn, :phoenix_endpoint, __MODULE__)
end
