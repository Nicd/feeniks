import Config

# Configures the endpoint
config :feeniks, Feeniks.Endpoint,
  render_errors: [accepts: ~w(html json)],
  pubsub_server: Feeniks.PubSub,
  code_reloader: config_env() == :dev,
  debug_errors: config_env() == :dev,
  check_origin: config_env() == :prod

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Do not print debug messages in production
if config_env() == :prod do
  config :logger, level: :error
end
