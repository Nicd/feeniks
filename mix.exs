defmodule Feeniks.MixProject do
  use Mix.Project

  def project do
    [
      app: :feeniks,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Feeniks.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:phoenix, "~> 1.6.15"},
      {:bandit, "~> 0.6.7"},
      {:jason, "~> 1.4"},
      {:phoenix_live_reload, "~> 1.4"}
    ]
  end
end
