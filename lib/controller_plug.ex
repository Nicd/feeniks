defmodule ControllerPlug do
  @behaviour Plug

  @impl Plug
  def init(action), do: action

  @impl Plug
  def call(conn, action) do
    action.(conn, conn.params)
  end
end
